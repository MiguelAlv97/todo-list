package com.example.sebastian.todo_list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference

class ListSelectionRecyclerViewAdapter(ref: DatabaseReference, val clickListener: ListSelectionRecyclerViewClickListener):
        RecyclerView.Adapter<ListSelectionRecyclerViewHolder>() {

    interface ListSelectionRecyclerViewClickListener {
        fun listitemClicked(todoList: TodoList)

    }

  val todolists:MutableList<TodoList> = mutableListOf()
  init {
      //ref.addListenerForSingleValueEvent(object : Child)
      val addChildEventListener = ref.addChildEventListener(object : ChildEventListener {
          override fun onCancelled(item: DatabaseError) {

          }

          override fun onChildMoved(item: DataSnapshot, p1: String?) {

          }

          override fun onChildChanged(item: DataSnapshot, p1: String?) {

          }

          override fun onChildAdded(item: DataSnapshot, p1: String?) {
              val listTitle = item.child("list-name").value.toString()
              val listId = item.key.toString()
              todolists.add(TodoList(listId, listTitle))
              //notifyDataSetChanged()
              notifyItemInserted(todolists.size)
          }

          override fun onChildRemoved(item: DataSnapshot) {
              val deletedIndex = todolists.indexOfFirst { it.id == item.key } //el it es lo mismo que element -> element.id
              todolists.removeAt(deletedIndex)
              notifyItemRemoved(deletedIndex)
          }


      })
  }

  override fun onCreateViewHolder(
          parent: ViewGroup,
          viewType: Int): ListSelectionRecyclerViewHolder {

    val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_selection_view_holder, parent, false)

    return ListSelectionRecyclerViewHolder(view)

  }


  override fun getItemCount(): Int {
    return todolists.count()
  }


  override fun onBindViewHolder(
    holder: ListSelectionRecyclerViewHolder,
    position: Int) {

    holder.listTitle.text = todolists[position].listName

    holder.itemView.setOnClickListener{
      clickListener.listitemClicked(todolists[position])
      }

  }
}












